﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
/**
 *  Алгоритм RSA состоит из следующих пунктов:
 *  
 *  1) Выбрать простые числа p и q
 *  2) Вычислить n = p * q
 *  3) Вычислить m = (p - 1) * (q - 1)
 *  4) Выбрать число d взаимно простое с m
 *  5) Выбрать число e так, чтобы e * d = 1 (mod m) 
 *  
 *  Числа e и d являются ключами RSA.
 *  Шифруемые данные необходимо разбить на блоки - числа от 0 до n - 1.
 *  Шифрование и дешифровка данных производятся следующим образом:
 *  
 *  > Шифрование: b = ae (mod n)
 *  > Дешифровка: a = bd (mod n)
 * 
 */
namespace RSA_encoder
{
    class Program
    {
        static Int64 modulepow(Int64 powareble, Int64 pow, Int64 module)
        {

            Int64 result = 1;
            while (pow>2)
            {
                if (pow % 2 == 0)
                {
            //a = (a ** 2) % m
            //p = p // 2
                    powareble = powareble*powareble;
                    pow = pow / 2;
                }
                else
                {
                    result = (result * powareble) % module;
                    pow--; 
                }
    //a = (a ** p) % m
    //result = (result * a) % m
    //return result
                Int64 p = powareble;
                for (int i = 0; i < pow; i++) powareble = powareble * p;
                    powareble = powareble % module;
                    result = (result * powareble) % module;
                    return result;
            }

            return -1;
        }
        static Int64 findE(Int64 a, Int64 b)
        {

//            Псевдокод.

//НА ВХОДЕ: два неотрицательных числа a и b: a>=b
//НА ВЫХОДЕ: d=НОД(a,b) и целые x,y: ax + by = d.

//1. Если b=0 положить d:=a, x:=1, y:=0 и возвратить (d,x,y)
//2. Положить x2:=1, x1:=0, y2:=0, y1:=1
//3. Пока b>0
//    3.1 q:=[a/b], r:=a-qb, x:=x2-qx1, y:=y2-qy1
//    3.2 a:=b, b:=r, x2:=x1, x1:=x, y2:=y1, y1:=y
//4. Положить d:=a, x:=x2, y:=y2 и возвратить (d,x,y)
//Исходник на Си.

///* Author:  Pate Williams (c) 1997 */

//#include <stdio.h>

//#define DEBUG


//void extended_euclid(long a, long b, long *x, long *y, long *d)

///* calculates a * *x + b * *y = gcd(a, b) = *d */

//{

//  long q, r, x1, x2, y1, y2;


//  if (b == 0) {

//    *d = a, *x = 1, *y = 0;

//    return;

//  }

//  x2 = 1, x1 = 0, y2 = 0, y1 = 1;

//  #ifdef DEBUG
//  printf("------------------------------");
//  printf("-------------------\n");
//  printf("q    r    x    y    a    b    ");
//  printf("x2   x1   y2   y1\n");
//  printf("------------------------------");
//  printf("-------------------\n");
//  #endif

//  while (b > 0) {

//    q = a / b, r = a - q * b;

//    *x = x2 - q * x1, *y = y2 - q * y1;

//    a = b, b = r;

//    x2 = x1, x1 = *x, y2 = y1, y1 = *y;

//    #ifdef DEBUG
//    printf("%4ld %4ld %4ld %4ld ", q, r, *x, *y);
//    printf("%4ld %4ld %4ld %4ld ", a, b, x2, x1);
//    printf("%4ld %4ld\n", y2, y1);
//    #endif

//  }

//  *d = a, *x = x2, *y = y2;

//  #ifdef DEBUG
//  printf("------------------------------");
//  printf("-------------------\n");
//  #endif

//}



//int main(void)
//{

//  long a = 4864, b = 3458, d, x, y;

//  extended_euclid(a, b, &x, &y, &d);

//  printf("x = %ld y = %ld d = %ld\n", x, y, d);

//  return 0;
//}
//Алгоритм работает за O(log2n) операций.

            Int64 r = 1;
            Int64 E11 =1, E12=0,E21=0,E22=1; // 1 0
            Int64 T11, T12, T21, T22;        // 0 1
            while (r>0)
            {
                r = a % b;

                T11 = E11;
                T12 = E12;
                T21 = E21;
                T22 = E22;


                E11 = T11 * 0 + T12 * 1;
                E12 = T11 * 1 + T12 * -a/b;
                E21 = T21 * 0 + T22 * 1;
                E22 = T21 * 1 + T22 * -a/b;

                a = b;
                b = r;
                
            }

            return E22;
        }
        static Int64 NOD(Int64 a, Int64 b)
        {
            Int64 r = a % b;
            while (r>0)
            {
                a = b;
                b = r; 
                r = a % b; 
            }
            return b;
        }

        static Int64 nextSimple(Int64 startKey, int k)
        {
            Random rnd = new Random(Convert.ToInt32(DateTime.Now.Millisecond));

            int ofst = rnd.Next((int)startKey);
            startKey += (UInt32)ofst;
            while (startKey % 2 == 0 || startKey % 5 == 0) startKey++;
#if DEBUG
            //Debug		 
            Console.WriteLine("ofst :{0}", ofst);
            Console.WriteLine("startKey :{0}", startKey);
#endif
            //Int64 N = startKey;
            //Int64 s=0, t=0;
            //Int64 a=0;
            //bool simple = false;

            //while(!simple)
            //{
            //    for (int i = 0; i < k; i++)
            //    {
            //        a = (Int64)rnd.Next((int)N);
            //        if (N % a == 0) { N++; break; }

            //        if ( Math.Pow(a,t)==1 % N);

            //        if (i == k - 1) simple = !simple;
            //    }

            //}

            //

            while (!prime.IsPrimes[startKey]) startKey++;

            return startKey;
        }
        static Int64 initBytes(int k)
        {
            string s = "";
            for (int i = 0; i < k; i++) s += '1';
#if DEBUG
            //Debug
            Console.WriteLine("bit combo: {0}", s);
#endif
            return Convert.ToInt64(s, 2);
        }

        static void Main(string[] args)
        {
#if DEBUG
            Console.WriteLine("Программа реализующая алгоритм шифрования RSA\nАвтор: Бесхмельницкий Антон Андреевич\nmail-to::Beskhmelnitsky.Anton@gmail.com\n\n#DEBUG MODE#\n");
#endif
            //Предлогаем пользователю выбрать кол-во бит для ключей
            //т.е. 2^k где n  от 1 до 30Вп
            //и генерируем два случайных простых числа не меньше указанной разрядности

            Console.WriteLine("Введите k для каждого из ключей (от 1 до 30),\nгде кол-во разрядов ключей будет не меньше 2^k\n\nВведите k:");
            int k = int.Parse(Console.ReadLine());
            if (k < 1 || k > 30) { Console.WriteLine("OUT OF RANGE!!!"); return; }
            Int64 startKey = initBytes(k);
#if DEBUG
            //Debug
            Console.WriteLine("startKey: {0}", startKey);
#endif
            //1) Выбрать простые числа p и q

            prime pr = new prime(startKey*3 );

            Int64 p = nextSimple(startKey, k);
            Int64 q = nextSimple(startKey, k);

#if DEBUG
            //Debug
            Console.WriteLine("p :{0}", p);
            Console.WriteLine("q :{0}", q);
#endif
            //2) Вычислить n = p * q
            Int64 n = p * q;

#if DEBUG
            //Debug
            Console.WriteLine("n :{0}", n);
#endif
            //3) Вычислить m = (p - 1) * (q - 1)
            Int64 m = (p - 1) * (q - 1);

#if DEBUG
            //Debug
            Console.WriteLine("m :{0}", m);
#endif
            //4) Выбрать число d взаимно простое с m
            long d = 3;
            while ( NOD(m, d) != 1 ) d++;
#if DEBUG
            //DebugInt64
            Console.WriteLine("d :{0}", d);
#endif
            //5) Выбрать число e так, чтобы e * d = 1 (mod m)
            //m * x + d * e = 1
            Int64 e = findE(m,d);

#if DEBUG
            //Debug
            Console.WriteLine("e :{0}",e);
#endif

            Console.WriteLine("Подготовка завершена\nОткрытый ключ :{0}\nЗакрытый ключ :{1}\nОбщая часть:{2}",e,d,n);
            Console.WriteLine("Введите шифруемый текст[20]:");
            string plainText = Console.ReadLine();
            if (plainText.Length > 20) {Console.WriteLine("ERRORR! OUT OF RANGE!!!"); return;}
            Console.WriteLine("------------------------------------------------------------------");
            Console.WriteLine("буква\tASCII-код\tCoded\tDecoded\tбуква");
            for (int i = 0; i < plainText.Length;i++ ) {
                Int64 coded = modulepow((int)plainText[i],e,n);
                Int64 decoded = modulepow(coded,d,n);
                         Console.WriteLine("{0}\t{1}\t\t{2}\t{3}\t{4}",plainText[i],(int)plainText[i],coded,decoded,(char)decoded);
        }
            Console.WriteLine("");
            Console.ReadLine();
        }
    }
}
